async function signUp() {
	console.log(document.signup.password.value);
	const data = {
		username: document.signup.name.value,
		email: document.signup.email.value,
		password: document.signup.password.value,
		roles: getSelectValues(document.signup.roles),
	};
	const result = await fetch(`http://localhost:8080/v1/auth/signup`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json;charset=utf-8',
			'Authorization': 'Bearer ' + getCookie('jwt'),
		},
		body: JSON.stringify(data),
	});

	if (result.ok) {
		console.log('signin');
	} else {
		alert('error');
	}
}

async function signIn() {
	setCookie('jwt', '');
	const data = {
		username: document.signin.name.value,
		password: document.signin.password.value,
	};
	const result = await fetch(`http://localhost:8080/v1/auth/signin`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json;charset=utf-8',
		},
		body: JSON.stringify(data),
	});
	if (result.ok) {
		let response = await result.json();
		console.log(response.token);
		setCookie('jwt', response.token);
		document.location.href = `http://localhost:${window.location.port}/server/front/home.html`
	} else {
		alert('error');
	}
}

function getCookie(name) {
	let matches = document.cookie.match(
		new RegExp(
			'(?:^|; )' +
				name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') +
				'=([^;]*)'
		)
	);
	return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options = {}) {
	options = {
		path: '/',
		// при необходимости добавьте другие значения по умолчанию
		...options,
	};

	if (options.expires instanceof Date) {
		options.expires = options.expires.toUTCString();
	}

	let updatedCookie =
		encodeURIComponent(name) + '=' + encodeURIComponent(value);

	for (let optionKey in options) {
		updatedCookie += '; ' + optionKey;
		let optionValue = options[optionKey];
		if (optionValue !== true) {
			updatedCookie += '=' + optionValue;
		}
	}

	document.cookie = updatedCookie;
}

function getSelectValues(select) {
	var result = [];
	var options = select && select.options;
	var opt;

	for (var i = 0, iLen = options.length; i < iLen; i++) {
		opt = options[i];

		if (opt.selected) {
			result.push(opt.value || opt.text);
		}
	}
	return result;
}

async function logout() {
	setCookie('jwt', '')
	document.location.href = `http://localhost:${window.location.port}/server/front`
}