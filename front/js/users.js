async function getUsers() {
	const result = await fetch('http://localhost:8080/v1/users', {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json;charset=utf-8',
			'Authorization': 'Bearer ' + getCookie('jwt'),
		},
	});
	if (result.ok) {
		let users = await result.json();
		users = users.usersList;
		const table = document.querySelector('tbody');
		users.forEach((user) => {
			const newElement = document.createElement('tr');
			newElement.innerHTML = `
			<td>${user.id}</td>
			<td><input name="name${user.id}" value="${user.username}"></td>
			<td><button onclick="deleteUser(${user.id})">delete user</button></td>
			<td><button onclick="updateUser(${user.id})">update user</button></td>`;
			table.appendChild(newElement);
		});
	} else {
		if(result.status === 403) {
			alert('forbidden error')
		} else {
			alert('error');
		}
	}
}
getUsers();

async function deleteUser(id) {
	const result = await fetch(`http://localhost:8080/v1/users/${id}`, {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json;charset=utf-8',
			'Authorization': 'Bearer ' + getCookie('jwt'),
		},
	});
	if (result.ok) {
		window.location.reload();
	} else {
		if(result.status === 403) {
			alert('forbidden error')
		} else {
			alert('error');
		}
	}
}

async function updateUser(id) {
	const data = {
		username: document.users[`name${id}`].value
	}
	const result = await fetch(`http://localhost:8080/v1/users/${id}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json;charset=utf-8',
			'Authorization': 'Bearer ' + getCookie('jwt'),
		},
		body: JSON.stringify(data),
	});

	if (result.ok) {
		window.location.reload();
	} else {
		if(result.status === 403) {
			alert('forbidden error')
		} else {
			alert('error');
		}
	}
}

function getCookie(name) {
	let matches = document.cookie.match(
		new RegExp(
			'(?:^|; )' +
				name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') +
				'=([^;]*)'
		)
	);
	return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options = {}) {
	options = {
		path: '/',
		// при необходимости добавьте другие значения по умолчанию
		...options,
	};

	if (options.expires instanceof Date) {
		options.expires = options.expires.toUTCString();
	}

	let updatedCookie =
		encodeURIComponent(name) + '=' + encodeURIComponent(value);

	for (let optionKey in options) {
		updatedCookie += '; ' + optionKey;
		let optionValue = options[optionKey];
		if (optionValue !== true) {
			updatedCookie += '=' + optionValue;
		}
	}

	document.cookie = updatedCookie;
}

async function logout() {
	setCookie('jwt', '')
	document.location.href = `http://localhost:${window.location.port}/server/front`
}

document.addEventListener('DOMContentLoaded', () => {

	const getSort = ({ target }) => {
			const order = (target.dataset.order = -(target.dataset.order || -1));
			const index = [...target.parentNode.cells].indexOf(target);
			const collator = new Intl.Collator(['en', 'ru'], { numeric: true });
			const comparator = (index, order) => (a, b) => order * collator.compare(
					a.children[index].innerHTML,
					b.children[index].innerHTML
			);
			
			for(const tBody of target.closest('table').tBodies)
					tBody.append(...[...tBody.rows].sort(comparator(index, order)));

			for(const cell of target.parentNode.cells)
					cell.classList.toggle('sorted', cell === target);
	};
	
	document.querySelectorAll('.table_sort thead').forEach(tableTH => tableTH.addEventListener('click', () => getSort(event)));
	
});