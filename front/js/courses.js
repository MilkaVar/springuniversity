async function getCourses() {
	const paramsString = document.location.search;
	const searchParams = new URLSearchParams(paramsString);
	const id = searchParams.get("id");
	const result = await fetch(`http://localhost:8080/v1/inner_entities/${id}`, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json;charset=utf-8',
			'Authorization': 'Bearer ' + getCookie('jwt'),
		},
	});
	if (result.ok) {
		let courses = await result.json();
		courses = courses.entities;
		const table = document.querySelector('tbody');
		console.log('/////////////////');
		courses.forEach((course) => {
			console.log(course.id);
			const newElement = document.createElement('tr');
			newElement.innerHTML = `
			<td>${course.id}</td>
			<td><input name="name${course.id}" value=${course.name}></td>
			<td><button onclick="deleteCourse(${course.id})">delete course</button></td>
			<td><button onclick="updateCourse(${course.id})">update course</button></td>`;
			table.appendChild(newElement);
		});
	} else {
		if(result.status === 403) {
			alert('forbidden error')
		} else {
			alert('error');
		}
	}
}
getCourses();

async function deleteCourse(id) {
	const result = await fetch(`http://localhost:8080/v1/inner_entities/${id}`, {
		method: 'DELETE',
		headers: {
			'Content-Type': 'application/json;charset=utf-8',
			'Authorization': 'Bearer ' + getCookie('jwt'),
		},
	});
	if (result.ok) {
		window.location.reload();
	} else {
		if(result.status === 403) {
			alert('forbidden error')
		} else {
			alert('error');
		}
	}
	
}

async function updateCourse(id) {
	id = +id
	const paramsString = document.location.search;
	const searchParams = new URLSearchParams(paramsString);
	const basicEntityId = searchParams.get("id");
	const data = {
		id,
		name: document.courses[`name${id}`].value,
		basicEntityId,
	}
	const url = `http://localhost:8080/v1/inner_entities/${id}`
	const result = await fetch(url, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json;charset=utf-8',
			'Authorization': 'Bearer ' + getCookie('jwt'),
		},
		body: JSON.stringify(data),
	});

	if (result.ok) {
		window.location.reload();
	} else {
		if(result.status === 403) {
			alert('forbidden error')
		} else {
			alert('error');
		}
	}
}

async function createCourse() {
	const paramsString = document.location.search;
	const searchParams = new URLSearchParams(paramsString);
	const id = +searchParams.get("id");
	const data = {
		basicEntityId: id,
		name: document.create.name.value
	}
	const result = await fetch(`http://localhost:8080/v1/inner_entities`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json;charset=utf-8',
			'Authorization': 'Bearer ' + getCookie('jwt'),
		},
		body: JSON.stringify(data),
	});

	if (result.ok) {
		window.location.reload();
	} else {
		if(result.status === 403) {
			alert('forbidden error')
		} else {
			const res = await result.json()
			alert(res.error);
		}
	}
}

function getCookie(name) {
	let matches = document.cookie.match(
		new RegExp(
			'(?:^|; )' +
				name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') +
				'=([^;]*)'
		)
	);
	return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options = {}) {
	options = {
		path: '/',
		// при необходимости добавьте другие значения по умолчанию
		...options,
	};

	if (options.expires instanceof Date) {
		options.expires = options.expires.toUTCString();
	}

	let updatedCookie =
		encodeURIComponent(name) + '=' + encodeURIComponent(value);

	for (let optionKey in options) {
		updatedCookie += '; ' + optionKey;
		let optionValue = options[optionKey];
		if (optionValue !== true) {
			updatedCookie += '=' + optionValue;
		}
	}

	document.cookie = updatedCookie;
}

async function logout() {
	setCookie('jwt', '')
	document.location.href = `http://localhost:${window.location.port}/server/front`
}

document.addEventListener('DOMContentLoaded', () => {

	const getSort = ({ target }) => {
			const order = (target.dataset.order = -(target.dataset.order || -1));
			const index = [...target.parentNode.cells].indexOf(target);
			const collator = new Intl.Collator(['en', 'ru'], { numeric: true });
			const comparator = (index, order) => (a, b) => order * collator.compare(
					a.children[index].innerHTML,
					b.children[index].innerHTML
			);
			
			for(const tBody of target.closest('table').tBodies)
					tBody.append(...[...tBody.rows].sort(comparator(index, order)));

			for(const cell of target.parentNode.cells)
					cell.classList.toggle('sorted', cell === target);
	};
	
	document.querySelectorAll('.table_sort thead').forEach(tableTH => tableTH.addEventListener('click', () => getSort(event)));
	
});