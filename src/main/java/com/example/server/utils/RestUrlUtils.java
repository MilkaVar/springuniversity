package com.example.server.utils;

public final class RestUrlUtils {
    public static final String BASIC_ENTITY_ID_PARAM = "basic_entity_id";
    public static final String BASIC_ENTITIES = "/basic_entities";
    public static final String BASIC_ENTITY_ID = "/{basic_entity_id}";
    public static final String BASIC_ENTITY_BASIC_ENTITY_ID = BASIC_ENTITIES + BASIC_ENTITY_ID;
    public static final String WHITESPACE_SPLITTER = " ";

    public static final String INNER_ENTITIES = "/inner_entities";
    public static final String INNER_ENTITY_ID = "/{inner_entity_id}";
    public static final String INNER_ENTITY_ID_PARAM = "inner_entity_id";

    public static final String USERS = "/users";
    public static final String USER_ID = "/{user_id}";
    public static final String USER_ID_PARAM = "user_id";



    public static final String BEARER = "bearer";
    public static final String AUTHORIZATION = "Authorization";
    public static final String OPERATION_NOT_ALLOWED = "Operation not allowed";

    public static final String ENTITY_WAS_NOT_FOUND = "BasicEntity with id=%s not found";
}
