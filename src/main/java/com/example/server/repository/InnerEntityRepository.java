package com.example.server.repository;

import com.example.server.model.InnerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InnerEntityRepository extends JpaRepository<InnerEntity, Long> {
    @Query("select o from InnerEntity o where o.basicEntity.id = :id")
    List<InnerEntity> getInnerEntityForBasicEntity(@Param("id") Long id);

    @Modifying
    @Query("update InnerEntity e set e.name = :name where e.innerId = :id")
    void update(@Param("id") Long id, @Param("name") String name);
}
