package com.example.server.repository;

import com.example.server.model.BasicEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BasicEntityRepository extends JpaRepository<BasicEntity, Long> {
    BasicEntity findByName(String name);

    @Modifying
    @Query("update BasicEntity e set e.name = :name where e.id = :id")
    void update(@Param("id") Long id, @Param("name") String name);

    @Query(value = "SELECT o from BasicEntity o " +
        "where o.id in :ids")
    List<BasicEntity> findAllByIds(List<Integer> ids);
}
