package com.example.server.controller;

import com.example.server.dto.BasicEntityDto;
import com.example.server.dto.EntityListDto;
import com.example.server.exception.EntityNotFoundException;
import com.example.server.exception.OperationNotAllowedException;
import com.example.server.service.BasicEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static com.example.server.utils.RestUrlUtils.*;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/v1")
public class BasicController {
    @Autowired
    BasicEntityService basicEntityService;

    @GetMapping(BASIC_ENTITIES)
    public ResponseEntity<EntityListDto> getAllBasic() {
        return ResponseEntity.ok().body(
            new EntityListDto(basicEntityService.getList())
        );
    }

    @GetMapping(BASIC_ENTITY_BASIC_ENTITY_ID)
    public ResponseEntity<BasicEntityDto> getBasicById(@PathVariable(BASIC_ENTITY_ID_PARAM) Long id) throws EntityNotFoundException {
        return ResponseEntity.ok().body(
            new BasicEntityDto(basicEntityService.getById(id))
        );
    }

    @PostMapping(BASIC_ENTITIES)
    @ResponseStatus(value = HttpStatus.CREATED)
    @PreAuthorize("hasRole('MODERATOR')")
    public BasicEntityDto postBasic(@Validated @RequestBody BasicEntityDto basicEntityDto) throws OperationNotAllowedException {
       return new BasicEntityDto(basicEntityService.create(basicEntityDto));
    }

    @PutMapping(BASIC_ENTITY_BASIC_ENTITY_ID)
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("hasRole('MODERATOR')")
    public void putBasic(@Validated @PathVariable(BASIC_ENTITY_ID_PARAM)  Long serverId, @RequestBody BasicEntityDto basicEntityDto) {
        basicEntityService.update(serverId, basicEntityDto);
    }

    @DeleteMapping(BASIC_ENTITY_BASIC_ENTITY_ID)
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("hasRole('MODERATOR')")
    public void deleteBasic(@PathVariable(BASIC_ENTITY_ID_PARAM)  Long serverId) throws EntityNotFoundException {
        basicEntityService.delete(serverId);
    }
}
