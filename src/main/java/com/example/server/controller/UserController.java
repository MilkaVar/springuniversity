package com.example.server.controller;

import com.example.server.dto.UserDto;
import com.example.server.dto.UserListDto;
import com.example.server.exception.EntityNotFoundException;
import com.example.server.exception.OperationNotAllowedException;
import com.example.server.pojo.SignupRequest;
import com.example.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static com.example.server.utils.RestUrlUtils.*;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/v1")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping(USERS)
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<UserListDto> getAllUser() throws EntityNotFoundException {
        return ResponseEntity.ok().body(
            new UserListDto(userService.getList())
        );
    }

    @GetMapping(USERS + USER_ID)
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<UserDto> getUserById(@PathVariable(USER_ID_PARAM) Long id) throws EntityNotFoundException {
        return ResponseEntity.ok().body(
            new UserDto(userService.getById(id))
        );
    }

    @PostMapping(USERS)
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseStatus(value = HttpStatus.CREATED)
    public UserDto postUser(@Validated @RequestBody SignupRequest signupRequest) throws OperationNotAllowedException {
        return new UserDto(userService.create(signupRequest));
    }

    @PutMapping(USERS + USER_ID)
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("hasRole('ADMIN')")
    public void putUser(@Validated @PathVariable(USER_ID_PARAM)  Long id, @RequestBody UserDto userDto) {
        userService.update(id, userDto);
    }

    @DeleteMapping(USERS + USER_ID)
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteUser(@PathVariable(USER_ID_PARAM)  Long id) throws EntityNotFoundException {
        userService.delete(id);
    }
}
