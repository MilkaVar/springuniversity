package com.example.server.controller;

import com.example.server.dto.InnerEntityDto;
import com.example.server.dto.InnerEntityListDto;
import com.example.server.exception.EntityNotFoundException;
import com.example.server.exception.OperationNotAllowedException;
import com.example.server.service.InnerEntityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import static com.example.server.utils.RestUrlUtils.*;


@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/v1")
public class InnerController {

    @Autowired
    InnerEntityService innerEntityService;

    @GetMapping(INNER_ENTITIES + BASIC_ENTITY_ID)
    public ResponseEntity<InnerEntityListDto> getAllInner(@PathVariable (BASIC_ENTITY_ID_PARAM) Long id) throws EntityNotFoundException {
        return ResponseEntity.ok().body(
            new InnerEntityListDto(innerEntityService.getByBasicEntityId(id))
        );
    }

    @PostMapping(INNER_ENTITIES)
    @PreAuthorize("hasRole('MODERATOR')")
    @ResponseStatus(value = HttpStatus.CREATED)
    public InnerEntityDto postInner(@Validated @RequestBody InnerEntityDto innerEntityDto) throws OperationNotAllowedException {
        return new InnerEntityDto(innerEntityService.create(innerEntityDto));
    }

    @PutMapping(INNER_ENTITIES + INNER_ENTITY_ID)
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("hasRole('MODERATOR')")
    public void putInner(@Validated @PathVariable(INNER_ENTITY_ID_PARAM)  Long id, @RequestBody InnerEntityDto innerEntityDto) {
        innerEntityService.update(id, innerEntityDto);
    }

    @DeleteMapping(INNER_ENTITIES + INNER_ENTITY_ID)
    @ResponseStatus(value = HttpStatus.OK)
    @PreAuthorize("hasRole('MODERATOR')")
    public void deleteInner(@PathVariable(INNER_ENTITY_ID_PARAM)  Long id) throws EntityNotFoundException {
        innerEntityService.delete(id);
    }
}
