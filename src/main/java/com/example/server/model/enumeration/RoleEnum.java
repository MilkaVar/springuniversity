package com.example.server.model.enumeration;

public enum RoleEnum {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN
}
