package com.example.server.service.impl;

import com.example.server.dto.BasicEntityDto;
import com.example.server.exception.EntityNotFoundException;
import com.example.server.exception.OperationNotAllowedException;
import com.example.server.model.BasicEntity;
import com.example.server.repository.BasicEntityRepository;
import com.example.server.service.BasicEntityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.example.server.utils.RestUrlUtils.*;

@Service
@Transactional
@Slf4j
public class BasicEntityServiceImpl implements BasicEntityService {
    @Autowired
    BasicEntityRepository basicEntityRepository;

    @Cacheable("basicEntities")
    @Override
    public List<BasicEntity> getList() {
        log.info("Get all entity...");
        return basicEntityRepository.findAll();
    }

    @Override
    public BasicEntity getById(Long id) throws EntityNotFoundException {
        log.info("Get entity with id={}...", id);
        Optional<BasicEntity> entity = basicEntityRepository.findById(id);
        if (entity.isEmpty()) {
            throw new EntityNotFoundException(String.format(ENTITY_WAS_NOT_FOUND, id));
        }
        return entity.get();
    }

    @Override
    public BasicEntity create(BasicEntityDto basicEntityDto) throws OperationNotAllowedException {
        if (basicEntityDto.getId() != null) {
            throw new OperationNotAllowedException(OPERATION_NOT_ALLOWED);
        }
        log.info("Create entity...");
        return basicEntityRepository.save(fillEntity(basicEntityDto));
    }

    @Override
    public void update(Long id, BasicEntityDto basicEntityDto) {
        basicEntityDto.setId(id);
        log.info("Update entity with ip={}...", id);
        basicEntityRepository.update(basicEntityDto.getId(), basicEntityDto.getName());
    }

    @Override
    public void delete(Long id) throws EntityNotFoundException {
        log.info("Delete entity with ip={}...", id);
        basicEntityRepository.delete(getById(id));
    }

    private BasicEntity fillEntity(BasicEntityDto basicEntityDto) {
        BasicEntity basicEntity = new BasicEntity();
        basicEntity.setId(basicEntityDto.getId());
        basicEntity.setName(basicEntityDto.getName());
        return basicEntity;
    }
}
