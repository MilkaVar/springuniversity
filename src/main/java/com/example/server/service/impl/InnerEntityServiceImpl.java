package com.example.server.service.impl;

import com.example.server.dto.InnerEntityDto;
import com.example.server.exception.EntityNotFoundException;
import com.example.server.exception.OperationNotAllowedException;
import com.example.server.model.InnerEntity;
import com.example.server.repository.BasicEntityRepository;
import com.example.server.repository.InnerEntityRepository;
import com.example.server.service.InnerEntityService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.example.server.utils.RestUrlUtils.ENTITY_WAS_NOT_FOUND;
import static com.example.server.utils.RestUrlUtils.OPERATION_NOT_ALLOWED;

@Service
@Transactional
@Slf4j
public class InnerEntityServiceImpl implements InnerEntityService {

    @Autowired
    InnerEntityRepository innerEntityRepository;
    @Autowired
    BasicEntityRepository basicEntityRepository;

    @Cacheable("innerEntities")
    @Override
    public List<InnerEntity> getList() {
        log.info("Get all entity...");
        return innerEntityRepository.findAll();
    }

    @Override
    public InnerEntity getById(Long id) throws EntityNotFoundException {
        log.info("Get entity with id={}...", id);
        Optional<InnerEntity> entity = innerEntityRepository.findById(id);
        if (entity.isEmpty()) {
            throw new EntityNotFoundException(String.format(ENTITY_WAS_NOT_FOUND, id));
        }
        return entity.get();
    }

    @Override
    public List<InnerEntity> getByBasicEntityId(Long id) throws EntityNotFoundException {
        log.info("Get entity with basic entity id={}...", id);
            return innerEntityRepository.getInnerEntityForBasicEntity(id);
    }

    @Override
    public InnerEntity create(InnerEntityDto innerEntityDto) throws OperationNotAllowedException {
        if (innerEntityDto.getId() != null) {
            throw new OperationNotAllowedException(OPERATION_NOT_ALLOWED);
        }
        log.info("Create entity...");
        return innerEntityRepository.save(fill(innerEntityDto));
    }

    @Override
    public void update(Long id, InnerEntityDto innerEntityDto) {
        innerEntityDto.setId(id);
        log.info("Update entity with ip={}...", id);
        innerEntityRepository.update(innerEntityDto.getId(), innerEntityDto.getName());
    }

    @Override
    public void delete(Long id) throws EntityNotFoundException {
        log.info("Delete entity with ip={}...", id);
        innerEntityRepository.delete(getById(id));
    }

    private InnerEntity fill(InnerEntityDto dto) {
        InnerEntity inner = new InnerEntity();
        inner.setInnerId(dto.getId());
        inner.setName(dto.getName());
        inner.setBasicEntity(basicEntityRepository.getById(dto.getBasicEntityId()));
        return inner;
    }
}
