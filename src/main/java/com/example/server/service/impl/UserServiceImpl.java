package com.example.server.service.impl;

import com.example.server.dto.UserDto;
import com.example.server.exception.EntityNotFoundException;
import com.example.server.exception.OperationNotAllowedException;
import com.example.server.model.Role;
import com.example.server.model.User;
import com.example.server.model.enumeration.RoleEnum;
import com.example.server.pojo.SignupRequest;
import com.example.server.repository.RoleRepository;
import com.example.server.repository.UserRepository;
import com.example.server.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.example.server.utils.RestUrlUtils.ENTITY_WAS_NOT_FOUND;

@Service
@Transactional
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    RoleRepository roleRepository;

    @Cacheable("users")
    @Override
    public List<User> getList() {
        log.info("Get all users...");
        return userRepository.findAll();
    }

    @Override
    public User getById(Long id) throws EntityNotFoundException {
        log.info("Get user with id={}...", id);
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            throw new EntityNotFoundException(String.format(ENTITY_WAS_NOT_FOUND, id));
        }
        return user.get();
    }

    @Override
    public User create(SignupRequest signupRequest) throws OperationNotAllowedException {
        User user = new User(
            signupRequest.getUsername(),
            signupRequest.getEmail(),
            passwordEncoder.encode(signupRequest.getPassword()));

        Set<String> reqRoles = signupRequest.getRoles();
        Set<Role> roles = new HashSet<>();

        if (reqRoles == null) {
            Role userRole = roleRepository
                .findByName(RoleEnum.ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Error, Role USER is not found"));
            roles.add(userRole);
        } else {
            reqRoles.forEach(r -> {
                switch (r) {
                    case "admin":
                        Optional<Role> adminRole = roleRepository
                            .findByName(RoleEnum.ROLE_ADMIN);
                        adminRole.ifPresent(roles::add);
                        break;
                    case "mod":
                        Optional<Role> modRole = roleRepository
                            .findByName(RoleEnum.ROLE_MODERATOR);
                        modRole.ifPresent(roles::add);
                        break;
                    case "user":
                        Optional<Role> userRole = roleRepository
                            .findByName(RoleEnum.ROLE_USER);
                        userRole.ifPresent(roles::add);
                        break;
                }
            });
        }
        user.setRoles(roles);
        return userRepository.save(user);
    }

    @Override
    public void update(Long id, UserDto userDto) {
        userDto.setId(id);
        log.info("Update user with ip={}...", id);
        userRepository.update(id, userDto.getUsername());
    }

    @Override
    public void delete(Long id) throws EntityNotFoundException {
        log.info("Delete user with ip={}...", id);
        userRepository.delete(getById(id));
    }
}
