package com.example.server.service;

import com.example.server.dto.InnerEntityDto;
import com.example.server.exception.EntityNotFoundException;
import com.example.server.exception.OperationNotAllowedException;
import com.example.server.model.InnerEntity;

import java.util.List;

public interface InnerEntityService {
    List<InnerEntity> getList();
    InnerEntity getById(Long id) throws EntityNotFoundException;
    List<InnerEntity> getByBasicEntityId(Long id) throws EntityNotFoundException;
    InnerEntity create(InnerEntityDto innerEntityDto) throws OperationNotAllowedException;
    void update(Long id, InnerEntityDto innerEntityDto);
    void delete(Long id) throws EntityNotFoundException;
}
