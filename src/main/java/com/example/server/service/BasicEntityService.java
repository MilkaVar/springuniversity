package com.example.server.service;

import com.example.server.dto.BasicEntityDto;
import com.example.server.exception.EntityNotFoundException;
import com.example.server.exception.OperationNotAllowedException;
import com.example.server.model.BasicEntity;

import java.util.List;

public interface BasicEntityService {
    List<BasicEntity> getList();
    BasicEntity getById(Long id) throws EntityNotFoundException;
    BasicEntity create(BasicEntityDto basicEntityDto) throws OperationNotAllowedException;
    void update(Long id, BasicEntityDto basicEntityDto);
    void delete(Long id) throws EntityNotFoundException;
}
