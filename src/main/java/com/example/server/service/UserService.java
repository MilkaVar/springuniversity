package com.example.server.service;

import com.example.server.dto.UserDto;
import com.example.server.exception.EntityNotFoundException;
import com.example.server.exception.OperationNotAllowedException;
import com.example.server.model.User;
import com.example.server.pojo.SignupRequest;

import java.util.List;

public interface UserService {
    List<User> getList();
    User getById(Long id) throws EntityNotFoundException;
    User create(SignupRequest signupRequest) throws OperationNotAllowedException;
    void update(Long id, UserDto userDto);
    void delete(Long id) throws EntityNotFoundException;
}
