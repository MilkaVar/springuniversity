package com.example.server;

import com.example.server.model.Role;
import com.example.server.model.enumeration.RoleEnum;
import com.example.server.pojo.SignupRequest;
import com.example.server.repository.RoleRepository;
import com.example.server.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Collections;
import java.util.Set;

@SpringBootApplication
public class ServerApplication {
	public static void main(String[] args) {
		SpringApplication.run(ServerApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(UserService userService, RoleRepository roleRepository) {
		return args -> {
			if (roleRepository.findAll().size() == 0) {
				roleRepository.save(new Role(1L, RoleEnum.ROLE_USER));
				roleRepository.save(new Role(2L, RoleEnum.ROLE_MODERATOR));
				roleRepository.save(new Role(3L, RoleEnum.ROLE_ADMIN));
			}
			if (userService.getList().size() == 0)
				userService.create(new SignupRequest("admin", "admin@mail.ru", Set.of("admin"), "admin"));
		};
	}
}
