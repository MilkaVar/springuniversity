package com.example.server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class ParseTokenException extends Exception {

    public ParseTokenException() {
        super("FAILED_TO_PARSE_AUTHORIZATION_TOKEN");
    }
}