package com.example.server.dto;

import com.example.server.model.InnerEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class InnerEntityListDto {
    private List<InnerEntityDto> entities;

    public InnerEntityListDto(List<InnerEntity> entities) {
        List<InnerEntityDto> innerEntityDtos = new ArrayList<>();
        for(InnerEntity inner : entities) {
            innerEntityDtos.add(new InnerEntityDto(inner));
        }
        this.entities = innerEntityDtos;
    }
}
