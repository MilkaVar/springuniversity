package com.example.server.dto;

import com.example.server.model.User;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UserListDto {
    private List<UserDto> usersList;

    public UserListDto(List<User> users) {
        List<UserDto> userDtoList = new ArrayList<>();
        for(User user : users) {
            userDtoList.add(new UserDto(user));
        }
        this.usersList = userDtoList;
    }
}