package com.example.server.dto;

import com.example.server.model.BasicEntity;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class EntityListDto {
    private List<BasicEntityDto> entities;

    public EntityListDto(List<BasicEntity> entities) {
        List<BasicEntityDto> basicEntityDtos = new ArrayList<>();
        for(BasicEntity basicEntity : entities) {
            basicEntityDtos.add(new BasicEntityDto(basicEntity));
        }
        this.entities = basicEntityDtos;
    }
}
