package com.example.server.dto;

import com.example.server.model.BasicEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BasicEntityDto {
    private Long id;
    private String name;

    public BasicEntityDto(BasicEntity basicEntity) {
        this.id = basicEntity.getId();
        this.name = basicEntity.getName();
    }
}
