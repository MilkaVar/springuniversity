package com.example.server.dto;

import com.example.server.model.InnerEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InnerEntityDto {
    private Long id;
    private String name;
    private Long basicEntityId;

    public InnerEntityDto(InnerEntity inner) {
        this.id = inner.getInnerId();
        this.name = inner.getName();
        this.basicEntityId = inner.getBasicEntity().getId();
    }
}
